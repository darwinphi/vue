<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<style>
	[v-cloak] {
		display: none;
	}

	.active {
		background-color: crimson;
		transition: background-color 0.8s ease-out;
	}
</style>
	<body>
		
		<div id="app" v-cloak>
			<p>Todo: <input type="text" v-model="todo"></p>
			<button v-on:click="addTodo">Add Todo</button>
			<h1 >{{ message }}</h1>
			<ol>
		    <li v-for="todo in todos">
		      {{ todo.text }}
		    </li>
		  </ol>
		  
		  <hr>
			<button v-on:click="toggleStyle">Add Style</button>
			<p v-bind:class="{active: isActive}">Hello</p>

			<hr>
			
			<button v-on:click="loadUsers">Load Users</button>
			<button v-on:click="removeLastUser">Remove Last User</button>
			<button v-on:click="clearUsers">Clear Lists</button>

			<p>Total No. of Users: {{ totalUsers }}</p>
			<p>Total No. of ID: {{ totalID }}</p>

			<ol>
		    <li v-for="user in users">
		      Name: {{ user.name }}
					<span v-if="user.email === 'Sincere@april.biz'">- BANNED EMAIL</span>

					ID: <input type="number" name="" v-model.number="user.id">
					
					<button @click="user.id += 1">Add</button>
					<button @click="removeUser(user)">Remove</button>
		    </li>
		  </ol>
		</div>

		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js"></script> -->
		<script src="https://unpkg.com/vue"></script>
		<script src="index.js"></script>
	</body>
</html>