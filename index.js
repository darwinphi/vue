new Vue({
	el: "#app",
	data: {
		isActive: false,
		message: "Hello World",
		todo: '',
		todos: [
      { id: 1, text: 'Learn JavaScript' },
      { id: 2, text: 'Learn Vue' },
      { id: 3, text: 'Build something awesome' }
    ],
    users: [],
	},
	methods: {
		addTodo: function() {
			if (this.todo === '') {
				alert("Should not be empty")
			}
			else {
				this.todos = [...this.todos, { text: this.todo} ]
			}
		},
		toggleStyle: function() {
			this.isActive = !this.isActive
		},
		loadUsers: function() {
			fetch('http://jsonplaceholder.typicode.com/users/')
			.then(response => response.json())
			.then(json => {
				json.map(users => this.users.push(users))
			})
		},
		removeLastUser: function() {
			return this.users.pop()
		},
		removeUser: function(user) {
			var index = this.users.indexOf(user);
      this.users.splice(index, 1);
		},
		clearUsers: function() {
			return this.users.length === 0 ? alert("It's already empty") : this.users = [] 
		},
	},
	computed: {
		totalUsers() {
			return this.users.length
		},
		totalID() {
			return this.users.reduce((sum, user) => {
				return sum + user.id
			}, 0)
		},
	},
	created() {
		// fetch(api) 
		
	}
})

